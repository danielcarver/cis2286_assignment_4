<?php
/**
 * Author: Dan Carver
 * Date: 2017-02-20
 * Purpose: reads a text file that holds grades from an assignment, prints data to a table on a web page
 */

//vars and getting file opened and read
$fp = fopen("grades/scores2017.txt", "r");
$data = fread($fp, 4096);
$grades = array();
$grades = explode("\r\n", $data);
fclose($fp);
$highestGrade = 0;
$lowestGrade = 0;
$invalIditems = 0;
$sum = 0;
$length = 0;
$averageGrade = 0;
$invalidGrades = array();
$validGrades = array();
$between0And10 = 0;
$between11And20 = 0;
$between21And30 = 0;
$between31And40 = 0;
$between41And50 = 0;
$between51And60 = 0;
$between61And70 = 0;
$between71And80 = 0;
$between81And90 = 0;
$between91And100 = 0;

//foreach that tests each grade to find out if it is valid or not, and then increments whichever var needs to depending
//on what the grade is.
foreach ($grades as $grade )
{
    switch($grade)
    {
        case $grade < 0 || $grade > 100:
            array_push($invalidGrades, $grade);
            $invalIditems++;
            break;
        case $grade >= 0 && $grade <= 10:
            array_push($validGrades, $grade);
            $between0And10++;
            break;
        case $grade >= 11 && $grade <= 20:
            array_push($validGrades, $grade);
            $between11And20++;
            break;
        case $grade >= 21 && $grade <= 30:
            array_push($validGrades, $grade);
            $between21And30++;
            break;
        case $grade >= 31 && $grade <= 40:
            array_push($validGrades, $grade);
            $between31And40++;
            break;
        case $grade >= 41 && $grade <= 50:
            array_push($validGrades, $grade);
            $between41And50++;
            break;
        case $grade >= 51 && $grade <= 60:
            array_push($validGrades, $grade);
            $between51And60++;
            break;
        case $grade >= 61 && $grade <= 70:
            array_push($validGrades, $grade);
            $between61And70++;
            break;
        case $grade >= 71 && $grade <= 80:
            array_push($validGrades, $grade);
            $between71And80++;
            break;
        case $grade >= 81 && $grade <= 90:
            array_push($validGrades, $grade);
            $between81And90++;
            break;
        case $grade >= 91 && $grade <= 100:
            array_push($validGrades, $grade);
            $between91And100++;
            break;
    }
}

$sum = array_sum($validGrades);
$length = count($validGrades);
$averageGrade = $sum / $length;
$lowestGrade = $validGrades[0];

//determining the highest grade
for($i = 0; $i < $length; $i++)
{
    if($validGrades[$i] > $highestGrade)
    {
        $highestGrade = $validGrades[$i];
    }
}


//determining the lowest grade
for($i = 0; $i < $length; $i++)
{
    if($validGrades[$i] < $lowestGrade)
    {
        $lowestGrade = $validGrades[$i];
    }
}


?>
<!doctype html>
<html>
    <head>
        <title>Assignment 3 Results</title>
        <link href="lib/bootstrap-3.3.7-dist/css/bootstrap.css" rel="stylesheet">
        <style>
            body{background-color: #84d32a;}
            #container{width: 500px;
                padding: 20px;}
            .table{background-color: white;
                width: 400px;}
        </style>
    </head>
    <body>
        <div id="container">
            <h2>CIS2286 - Assignment 3 Results</h2>
            <h4>Grade Distribution</h4>
            <table class="table table-bordered">
                <th>Grade Range(%)</th>
                <th>Number of Scores</th>
                <tr>
                    <td>Between 0 and 10</td>
                    <td><?php echo $between0And10; ?></td>
                </tr>
                <tr>
                    <td>Between 11 and 20</td>
                    <td><?php echo $between11And20; ?></td>
                </tr>
                <tr>
                    <td>Between 21 and 30</td>
                    <td><?php echo $between21And30; ?></td>
                </tr>
                <tr>
                    <td>Between 31 and 40</td>
                    <td><?php echo $between31And40; ?></td>
                </tr>
                <tr>
                    <td>Between 41 and 50</td>
                    <td><?php echo $between41And50; ?></td>
                </tr>
                <tr>
                    <td>Between 51 and 60</td>
                    <td><?php echo $between51And60; ?></td>
                </tr>
                <tr>
                    <td>Between 61 and 70</td>
                    <td><?php echo $between61And70; ?></td>
                </tr>
                <tr>
                    <td>Between 71 and 80</td>
                    <td><?php echo $between71And80; ?></td>
                </tr>
                <tr>
                    <td>Between 81 and 90</td>
                    <td><?php echo $between81And90; ?></td>
                </tr>
                <tr>
                    <td>Between 91 and 100</td>
                    <td><?php echo $between91And100; ?></td>
                </tr>
                <tr>
                    <td>Invalid Grades</td>
                    <td><?php echo $invalIditems; ?></td>
                </tr>
            </table>


            <h4>Grade Metrics</h4>
            <table class="table table-bordered">
                <th>Metric</th>
                <th>Value</th>
                <tr>
                    <td>Average Grade</td>
                    <td><?php echo number_format($averageGrade, 2) . "%"; ?></td>
                </tr>
                <tr>
                    <td>Highest Grade</td>
                    <td><?php echo $highestGrade . "%"; ?></td>
                </tr>
                <tr>
                    <td>Lowest Grade</td>
                    <td><?php echo $lowestGrade . "%"; ?></td>
                </tr>
                <tr>
                    <td>Invalid Items</td>
                    <td>
                        <?php
                        //printing all of the invalid grades with a comma between ( no comma after, just for you :) )
                        for($i = 0; $i < count($invalidGrades); $i++)
                        {
                            echo $invalidGrades[$i];
                            if($i < count($invalidGrades) - 1)
                            {
                                echo ", ";
                            }
                        }
                    ?></td>
                </tr>
            </table>
        </div>
    </body>
</html>
